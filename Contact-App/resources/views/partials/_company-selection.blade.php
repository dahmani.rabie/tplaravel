{{--A l'appel de cette vue , il est imperatif de lui passer une liste
de compagnies $companies--}}
<form method="get" action="/contacts" id="formfilter">
    <select id="selectfilter" name="company_id">
        <option value="0"  >Selectionner companie</option>
        @foreach($companies as $companie)
            <option value="{{ $companie['id']}} ">{{ $companie['email']}}</option>
        @endforeach
    </select>
</form>
<script>
    document.getElementById('selectfilter').addEventListener(
        'change',function (){
        document.getElementById('formfilter').submit()
    })
</script>
