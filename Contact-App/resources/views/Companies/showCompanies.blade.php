{{--A l'appel de cette vue , il est imperatif de lui passer une liste
de compagnies $companies--}}

@extends('layouts.main')
@section('main')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">raison social</th>
            <th scope="col">email</th>
            <th scope="col">adresse</th>
        </tr>
        </thead>

        <tbody>
        @foreach($companies as $companie)
        <tr>
            <th scope="row">1</th>
            <td>{{$companie['raison social']}}</td>
            <td>{{$companie['email']}}</td>
            <td>{{$companie['adresse']}}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
