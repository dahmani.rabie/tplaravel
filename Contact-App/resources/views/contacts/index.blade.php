@extends('layouts.main')
@section('main')
<div>
    <div class="d-flex justify-content-end pe-2" >
        @include('partials._filter')
    </div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">First</th>
            <th scope="col">Last</th>
        </tr>
        </thead>
        <tbody>
        @foreach($contacts as $contact)
            <tr>
                <td>{{$contact['name']}}</td>
                <td>{{$contact['age']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>


</div>
@endsection
