{{--A l'appel de cette vue , il est imperatif de lui passer une liste
de compagnies $companies--}}
@extends('layouts.main')
@section('main')
    <form>
            <div class="mb-3 ">
                <table>
                    <tr>
                        <td>
                            <label for="exampleInputEmail1" class="form-label">first name</label>
                        </td>
                        <td>
                            <input type="text" class="form-control" id="first-name" aria-describedby="emailHelp">
                        </td>
                    </tr>
                </table>

            </div>
            <div class="mb-3 d-flex flex-row">
                <label for="exampleInputPassword1" class="form-label col-3">last name</label>
                <input type="text" class="form-control" id="last-name">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">email</label>
                <input type="email" class="form-control" id="email">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">phone</label>
                <input type="text" class="form-control" id="phone">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">adress</label>
                <input type="text" class="form-control" id="adress">
            </div>
            <label for="exampleInputPassword1" class="form-label">comapny</label>
            <select class="form-select" aria-label="Default select example" id="comapny">
                <option selected>Open this select menu</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
