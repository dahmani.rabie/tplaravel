<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactsController extends Controller
{
    //la méthode index
    public function index(Request $request){
        //dd((int)$request->input('company_id'));
        return view('contacts.index', [
            'contacts' => $this->getContacts((int)$request->input('company_id')),
            'companies' => $this->getCompanies()
        ]);

    }
    public  function create()
    {
        return view('contacts.create');
    }
    public function show($id)
    {
        return view('contacts.show',['id'=>$id]);
    }
    public function getContacts($id=null){
        $filteredContacts=[];
        $contacts=[
            [
                'name'=> 'abdelmoula',
                'age'=>30,
                'companie'=>1
            ],
            [
                'name'=> 'abouzia',
                'age'=>20,
                'companie'=>2
            ],
            [
                'name'=> 'Rabie',
                'age'=>20,
                'companie'=>3
            ]
        ];
        if ($id != null && $id != 0){
            foreach ($contacts as $contact){
                if($contact['companie']===$id){
                    $filteredContacts[]=$contact;
                }
            }
            return  $filteredContacts;
        }
        return $contacts;
    }

    public function getCompanies(){
        $companies=[
            [
                'id'=>1,
                'raison social'=> 'SARL',
                'email'=>'adidas@email.com',
                'adresse'=>'adidas'
            ],
            [
                'id'=>2,
                'raison social'=> 'SE',
                'email'=>'nike@email.com',
                'adresse'=>'nike'
            ],
            [
                'id'=>3,
                'raison social'=> 'SA',
                'email'=>'jordan@email.com',
                'adresse'=>'jordan'
            ]
        ];
        return $companies;
    }

}
