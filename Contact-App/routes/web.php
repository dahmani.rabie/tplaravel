<?php

use App\Http\Controllers\ContactsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('layouts.main');
});

Route::get('/contacts',[ContactsController::class, 'index']
)->name('contacts.index');

Route::get('/contacts/create',[ContactsController::class, 'create']
)->name('contacts. create');

Route::get('/contacts/{id}',[ContactsController::class,'show']
    )->name('contacts.show');

Route::get('/companies/ShowCompanies',function (){
    return view('Companies.showCompanies',['companies'=>getCompanies()]);
})->name('cmpanies.index');
function getContacts($id=null){
    $filteredContacts=[];
    $contacts=[
        [
            'name'=> 'abdelmoula',
            'age'=>30,
            'companie'=>1
        ],
        [
            'name'=> 'abouzia',
            'age'=>20,
            'companie'=>2
        ]
    ];
    if ($id !== null){

        foreach ($contacts as $contact){
            if($contact['companie']===$id){
                $filteredContacts[]=$contact;
            }
        }
        return  $filteredContacts;
    }
    return $contacts;
}

function getCompanies(){
    $companies=[
        [
            'id'=>1,
            'raison social'=> 'SARL',
            'email'=>'adidas@email.com',
            'adresse'=>'adidas'
        ],
        [
            'id'=>2,
            'raison social'=> 'SE',
            'email'=>'nike@email.com',
            'adresse'=>'nike'
        ],
        [
            'id'=>3,
            'raison social'=> 'SA',
            'email'=>'jordan@email.com',
            'adresse'=>'jordan'
        ]
    ];
    return $companies;
}
